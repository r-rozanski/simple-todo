module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            index: {
                cwd: '',
                src: ['index.html'],
                dest: 'dist',
                expand: true
            },
            fonts: {
                cwd: 'bower_components/font-awesome/fonts',
                src: ['*.*'],
                dest: 'dist/fonts',
                expand: true
            }
        },
        clean: {
            build: {
                src: [ 'dist' ]
            }
        },
        cssmin: {
            build: {
                files: {
                    'dist/css/style.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.css',
                        'bower_components/font-awesome/css/font-awesome.css',
                        'assets/css/style.css'
                    ]
                }
            }
        },
        uglify: {
            build: {
                options: {
                    mangle: false
                },
                files: {
                    'dist/js/code.js': [
                        'bower_components/jquery/dist/jquery.js',
                        'bower_components/angular/angular.js',
                        'app/js/app.js'
                    ]
                }
            }
        },
        useminPrepare: {
            html: 'index.html',
            options: {
                flow: {}
            }
        },
        usemin: {
            html: 'dist/index.html'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-usemin');

    grunt.registerTask('build',[
        'useminPrepare',
        'clean:build',
        'copy',
        'cssmin',
        'uglify',
        'usemin'
    ]);
};

