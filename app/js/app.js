var app = angular.module('todoApp', []);

app.controller("TasksCtrl", ['$scope', function ($scope) {
    $scope.tasks = [];
    $scope.taskName = '';

    $scope.addTask = function () {
        if ($scope.taskName !== '') {
            $scope.tasks.push({"title": $scope.taskName, "completed": "false"});
            $scope.taskName = '';
        }
        angular.element('input').focus();
    };

    $scope.toggleCompleted = function (index) {
        if ($scope.tasks[index].completed === 'true') {
            $scope.tasks[index].completed = 'false';
        } else {
            $scope.tasks[index].completed = 'true';
        }
    };

    $scope.removeTask = function (index) {
        $scope.tasks.splice(index, 1);
    };

}]);
